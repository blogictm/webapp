﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VanityInfo;
using VanityFactory;
using VanityService;
using CommonLibrary;
namespace VanityBAL
{
    public class UserBAL1
    {
        private IUserService GetLoginDAL()
        {
            VanityFactory.ConcreteFactory concretefactory = null;
            IUserService iuserservice = null;
            try
            {
                concretefactory = new VanityFactory.ConcreteFactory();
                iuserservice = concretefactory.GetUserDAL();


            }
            catch (Exception ex)
            {
                throw ex;
            }
            return iuserservice;
        }

        public IAGetResponseInfo<UserInfo> Authentication(IAPostRequestInfo<UserInfo> requestinfo)
        {
            IUserService iuserservice = null;
            IAGetResponseInfo<UserInfo> responseinfo = null;
            UserInfo userinfo = null;
            try
            {
                responseinfo = new IAGetResponseInfo<UserInfo>();
                userinfo = new UserInfo();
                iuserservice = GetLoginDAL();
                userinfo = iuserservice.Authentication(requestinfo.TObject);
                if (userinfo != null)
                {
                    responseinfo.TObject = userinfo;
                    responseinfo.TObject.status = ApplicationConstants.SUCC_STAT;
                }
                else
                {
                    responseinfo.TObject.status = ApplicationConstants.FAIL_STAT;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                iuserservice = null;
            }
            return responseinfo;
        }
    }
}
