﻿using System;
namespace VanityInfo
{
    public class BaseInfo
    {
        public DateTime CreatedOn { get; set; }
        public string strCreatedOn { get; set; }
        public string CreatedBy { get; set; }
        public DateTime ModifiedOn { get; set; }
        public string strModifiedOn { get; set; }
        public string ModifiedBy { get; set; }
        public Boolean IsDeleted { get; set; }
        public DateTime DeletedOn { get; set; }
        public string strDeletedOn { get; set; }
        public string DeletedBy { get; set; }
        public string IPAddress { get; set; }
        public Boolean IsActive { get; set; }
        public string CompanyID { get; set; }
        public string status { get; set; }
        public string setdata { get; set; }
    }
}