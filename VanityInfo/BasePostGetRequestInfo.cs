﻿using System.Collections.Generic;
namespace VanityInfo
{
    public class IAPostRequestInfo<T> : BaseInfo where T : class
    {
        public T TObject { get; set; }
        public List<T> TObjectLst { get; set; }

    }
    public class IAGetResponseInfo<T> : BaseInfo where T : class
    {
        public T TObject { get; set; }
        public List<T> TObjectLst { get; set; }
    }
}
