﻿namespace VanityInfo
{
    public class UserInfo : BaseInfo
    {
        public string UserName { get; set; }
        public string RoleID { get; set; }
        public string UserMobileNo { get; set; }
        public string UserEmailID { get; set; }
        public string UserPassword { get; set; }
        public string UserID { get; set; }
        public string UserProfileImg { get; set; }
    }
}