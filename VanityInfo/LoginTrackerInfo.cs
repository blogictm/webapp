﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VanityInfo
{
    public class LoginTrackerInfo:BaseInfo
    {
        public string LoginID { get; set; }
        public string LoginType { get; set; }
        public string LoginBy { get; set; }
        public DateTime LoginOn { get; set; }
        public string strLoginOn { get; set; }
        public string LoginTime { get; set; }
        public string LogoutTime { get; set; }
        public string Browser { get; set; }
    }
}
