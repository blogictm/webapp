﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VanityClass
{
    public class SessionInfo
    {
        public string UserId { get; set; }
        public string UserName { get; set; }
        public string CompanyID { get; set; }
        public string RoleID { get; set; }
        public string UserEmailID { get; set; }
    }
}