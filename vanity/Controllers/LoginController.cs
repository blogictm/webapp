﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using VanityInfo;
using VanityBAL;
using CommonLibrary;
using VanityClass;
using VanityModels;
namespace vanity.Controllers
{
    public class LoginController :  FilterController
    {
        public ActionResult Logout()
        {
            base.AbandonSession();
            return RedirectToAction("Index", "Home");
        }

        //[HttpGet]
        //public ActionResult Login()
        //{
        //    return View();
        //}

        [HttpPost]
        public ActionResult Login()
        {
            IAPostRequestInfo<UserInfo> requestinfo=null;
            IAGetResponseInfo<UserInfo> responseinfo = null;
            SessionInfo sessionInfo = null;
            UserBAL1 userbal = null;
            LoginModel model = new LoginModel();
            model.userinfo = new UserInfo();
            model.userinfo.UserName = "Admin";
            model.userinfo.UserPassword = "Admin";
            try
            {
                requestinfo = new IAPostRequestInfo<UserInfo>();
                responseinfo = new IAGetResponseInfo<UserInfo>();
                requestinfo.TObject = new UserInfo();
                requestinfo.TObject = model.userinfo;
                userbal = new UserBAL1();
                responseinfo = userbal.Authentication(requestinfo);
                if (responseinfo != null && responseinfo.TObject.status == ApplicationConstants.SUCC_STAT)
                {
                    //store values in session variable
                    sessionInfo = new SessionInfo();
                    sessionInfo.UserId = responseinfo.TObject.UserID;
                    sessionInfo.UserName = responseinfo.TObject.UserName;
                    base.SetSession(sessionInfo);
                    return RedirectToAction("Fees", "Fees");
                }
                else
                {
                    return Json(ApplicationConstants.FAIL_STAT, JsonRequestBehavior.AllowGet);
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}