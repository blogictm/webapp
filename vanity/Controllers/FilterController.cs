﻿using System.Collections.Generic;
using System.Web.Mvc;
using System.Web.Routing;
using VanityClass;
namespace vanity.Controllers
{
    public class FilterController : Controller
    {
        // GET: Application
        private const string LogOnSession = "SessionInfo";  //session index name
        private const string ErrorController = "Error";      //session independent controller
        private const string LogOnController = "Login";      //session independent and LogOn controller    
        private const string LogOnAction = "Login";          //action to rederect



        private bool IsNonSessionController(RequestContext requestContext)
        {
            var currentController = requestContext.RouteData.Values["controller"].ToString().ToLower();
            var nonSessionedController = new List<string>() { ErrorController.ToLower(), LogOnController.ToLower() };
            return nonSessionedController.Contains(currentController);
        }

        private void Rederect(RequestContext requestContext, string action)
        {
            requestContext.HttpContext.Response.Clear();
            requestContext.HttpContext.Response.Redirect(action);
            //RedirectToAction("Login", "Login");
            requestContext.HttpContext.Response.End();
        }

        protected bool HasSession()
        {
            return Session[LogOnSession] != null;
        }

        protected SessionInfo GetSession()
        {
            return (SessionInfo)this.Session[LogOnSession];
        }

        protected void SetSession(SessionInfo model)
        {
            Session[LogOnSession] = model;
        }

        protected void AbandonSession()
        {
            if (HasSession())
            {
                Session.Abandon();
            }
        }
	}
}