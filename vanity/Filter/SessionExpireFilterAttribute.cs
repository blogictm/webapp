﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
namespace vanity.Filter
{
    public class SessionExpireFilterAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {

            if (HttpContext.Current.Session["SessionInfo"] == null)
            {

                filterContext.Result =
                new RedirectToRouteResult(new RouteValueDictionary   
                {  
                    { "action", "Login" },  
                    { "controller", "Login" },  
                    { "returnUrl", filterContext.HttpContext.Request.RawUrl}  
                });
                return;
            }
            base.OnActionExecuting(filterContext);
        }
    }
}