﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommonLibrary
{
    public class ControllerNameConstants
    {
        public const string CONT_MASTER = "1";
        public const string CONT_DOCTOR = "0";
    }
    public class ActionNameConstants
    {
        public const string ACTI_SAVE_FEES = "1";
        public const string ACTI_GET_FEES = "0";
    }
    public class ApplicationConstants
    {
        public const string SUCC_STAT = "1";
        public const string FAIL_STAT = "0";
        public const string ERROR_04 = "E104";
        public const string ERROR_05 = "E105";
        public const string ERROR_06 = "E106";
        public const string DATE_FORMAT = "dd-MMM-yyyy";
        public const string DATE_FORMAT_WITH_TIME = "dd-MMM-yyyy_hh_mm_ss";
        public const string DATE_FORMAT_WITH_TIME_AMPM = "dd-MMM-yyyy hh:mm tt";
        public const string DATE_FORMAT_WITH_TIME_AMPM_2 = "dd-MMM-yyyy hh:mm:ss tt";
        public const string DATE_FORMAT_FOR_FILE_NAME = "dd_MMM_yyyy";
        public const string FILE_NOT_FOUND = "Invalid file or File is not available";
        public const string ODS_DATE_FORMAT_WITH_TIME = "dd-MMM-yyyy hh:mm:ss tt";
        public const string ODS_DATE_FORMAT = "dd-MM-yyyy";
        public const string ODS_DATE_FORMAT_WITH_ZONE = "yyyy-MM-ddThh:mm:ss tt zzz";
        public const string TRAC_FILE_PATH = @"D:\Datacomp\ETVikramSchedular_Data\Log.txt";
        public const string ORA_ERR_CODE = "E1000";
        public const string GEN_ERR_CODE = "E1100";
        public const string VALIDATION_ERR_CODE = "E1200";
    }
}
