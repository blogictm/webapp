﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommonLibrary
{
    public static class CommonFunctions
    {
        public static string getTimeStamp()
        {
            return Convert.ToString(DateTime.Now.Day) + Convert.ToString(DateTime.Now.Month) + Convert.ToString(DateTime.Now.Year) + Convert.ToString(DateTime.Now.Hour) + Convert.ToString(DateTime.Now.Minute) + Convert.ToString(DateTime.Now.Second) + Convert.ToString(DateTime.Now.Millisecond);
        }

        public static DateTime CreateDate(string pstrdate)
        {
            DateTime dt = DateTime.Now;
            try
            {
                string[] pstrsplitdate = new string[5];
                if (pstrdate.Contains("-"))
                {
                    pstrsplitdate = pstrdate.Split('-');
                }
                else if (pstrdate.Contains("/"))
                {
                    pstrsplitdate = pstrdate.Split('/');
                }
                int lintday = Convert.ToInt32(pstrsplitdate[0]);
                int lintmonth = Convert.ToInt32(pstrsplitdate[1]);
                int lintyear = Convert.ToInt32(pstrsplitdate[2].Split(' ')[0]);
                dt = new DateTime(lintyear, lintmonth, lintday);
            }
            catch (Exception)
            {
            }
            return dt;
        }
        public static string DateTimeSQLite(DateTime datetime)
        {
            string dateTimeFormat = "{0}-{1}-{2} {3}:{4}:{5}.{6}";
            return string.Format(dateTimeFormat, datetime.Year, datetime.Month, datetime.Day, datetime.Hour, datetime.Minute, datetime.Second, datetime.Millisecond);
        }
        public static DateTime CreateDateTime(string pstrdate)
        {
            DateTime dt = DateTime.Now;
            try
            {
                string[] pstrsplitdate = new string[5];
                if (pstrdate.Contains("-"))
                {
                    pstrsplitdate = pstrdate.Split('-');
                }
                else if (pstrdate.Contains("/"))
                {
                    pstrsplitdate = pstrdate.Split('/');
                }
                int lintday = Convert.ToInt32(pstrsplitdate[0]);
                int lintmonth = Convert.ToInt32(pstrsplitdate[1]);
                int lintyear = Convert.ToInt32(pstrsplitdate[2].Split(' ')[0]);

                string[] time = pstrsplitdate[2].Split(' ')[1].Split(':');

                int hh = Convert.ToInt32(time[0]);
                int mm = Convert.ToInt32(time[1]);

                //.int ss = Convert.ToInt32(time[2]);
                string ampm = pstrsplitdate[2].Split(' ')[2];

                if (ampm.ToLower() == "pm")
                {
                    if (hh != 12)
                        hh = hh + 12;
                }
                else
                {
                    if (hh == 12)
                        hh = 0;
                }


                dt = new DateTime(lintyear, lintmonth, lintday, hh, mm, 0);

                //dt = DateTime.Parse(pstrsplitdate[0] + "/" + pstrsplitdate[1] + "/" + pstrsplitdate[2], new CultureInfo("en-CA"));

                //dt = DateTime.ParseExact("7/3/2015 1:52:16 PM", "d/M/yyyy h:mm:ss tt", CultureInfo.InvariantCulture);

                //DateTime.TryParseExact(pstrdate, "dd/MM/yyyy hh:mm tt", CultureInfo.InvariantCulture, DateTimeStyles.None, out dt);
            }
            catch (Exception)
            {
            }
            return dt;
        }

        public static string FormatTimeDigit(string val)
        {
            val = val.PadLeft(2, '0');
            return val;
        }
        public static string FormatMonthText(int monthval)
        {
            string[] months = { "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec" };
            string month = string.Empty;
            month = (monthval - 1) > 0 ? months[(monthval - 1)] : months[0];
            return month;
        }

        public static DateTime StringToDatetime(string pstrDate)
        {
            string[] lstrDateList = null;
            if (pstrDate.Contains("-"))
            {
                lstrDateList = pstrDate.Split("-".ToCharArray());
            }
            else
            {
                lstrDateList = pstrDate.Split("/".ToCharArray());
            }

            string strDate = lstrDateList[1] + "/" + lstrDateList[0] + "/" + lstrDateList[2];
            DateTime value = new DateTime(Convert.ToInt32(lstrDateList[2]), Convert.ToInt32(lstrDateList[1]), Convert.ToInt32(lstrDateList[0]));


            return value;
        }

        public static string DatetimeToString(string pstrDate)
        {
            string[] lstrDateList = null;
            if (pstrDate.Contains("-"))
            {
                lstrDateList = pstrDate.Split("-".ToCharArray());
            }
            else
            {
                lstrDateList = pstrDate.Split("/".ToCharArray());
            }

            string strDate = lstrDateList[1] + "-" + lstrDateList[0] + "-" + lstrDateList[2];
            //DateTime value = new DateTime(Convert.ToInt32(lstrDateList[2]), Convert.ToInt32(lstrDateList[1]), Convert.ToInt32(lstrDateList[0]));


            return strDate;
        }

        public static int GetFreqVal(string pstrFrequency)
        {
            int lintFreqVal = pstrFrequency == "Half-Yearly" ? 2 : pstrFrequency == "Quarterly" ? 4 : pstrFrequency == "Monthly" ? 12 : 1;
            return lintFreqVal;
        }
    }
}
