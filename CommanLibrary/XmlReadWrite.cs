﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IHAarogyaWebAppCommonLibrary
{
    public class XmlReadWrite
    {
        internal XmlReadWrite()
        {

        }
        internal DataSet ReadXml(string xmlFilePath)
        {
            DataSet xmlReadDataset = null;
            try
            {
                xmlReadDataset = new DataSet();
                xmlReadDataset.ReadXml(xmlFilePath);
            }
            catch
            {
                throw;
            }

            return xmlReadDataset;
        }
        internal DataSet WriteXml(string xmlReadFilePath, string xmlWriteFilePath)
        {
            DataSet xmlWriteDataset = null;

            try
            {
                xmlWriteDataset = new DataSet();
                xmlWriteDataset.ReadXml(xmlReadFilePath);
                xmlWriteDataset.WriteXml(xmlWriteFilePath, XmlWriteMode.WriteSchema);
            }
            catch
            {
                throw;
            }
            return xmlWriteDataset;
        }
    }
}
