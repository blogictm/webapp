﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IHAarogyaWebAppCommonLibrary
{
    public static class ConversionFunctions
    {
        public static string GetStringValueFromObject(object value)
        {
            string strValue = string.Empty;
            if (value != DBNull.Value)
            {
                strValue = Convert.ToString(value);
            }

            return strValue;
        }

        public static int GetInt32ValueFromObject(object value)
        {
            int iValue = 0;
            if (value != DBNull.Value)
            {
                iValue = Convert.ToInt32(value);
            }

            return iValue;
        }

        public static long GetInt64ValueFromObject(object value)
        {
            long iValue = 0;
            if (value != DBNull.Value)
            {
                iValue = Convert.ToInt64(value);
            }

            return iValue;
        }

        public static DateTime GetDateTimeFromObject(object value)
        {
            DateTime dtvalue = DateTime.MinValue;
            if (value != DBNull.Value)
            {
                dtvalue = Convert.ToDateTime(value);
            }
            return dtvalue;
        }

        public static Double GetDoubleValueFromObject(object value)
        {
            Double dValue = 0;
            if (value != DBNull.Value)
            {
                dValue = Convert.ToDouble(value);
            }

            return dValue;
        }

        public static bool GetBoolValueFromObject(object value)
        {
            bool bValue = false;

            if (value != DBNull.Value)
            {
                bValue = Convert.ToInt16(value) == 0 ? false : true;
            }
            return bValue;
        }

        public static DateTime ConvertToDateTimeFromODSFormat(object dateTime)
        {
            DateTime dateValue;
            dateValue = new DateTime();
            string dateTimeString = string.Empty;
            string[] dateTimeParts = null;
            try
            {
                if (dateTime != null)
                {
                    dateTimeString = Convert.ToString(dateTime);
                    if (string.IsNullOrWhiteSpace(dateTimeString) == false)
                    {
                        dateValue = Convert.ToDateTime(dateTimeString);
                    }
                }
            }
            catch
            {

            }
            return dateValue;
        }
    }
}
