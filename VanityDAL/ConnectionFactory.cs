﻿using System.Data.SqlClient;
using System.Configuration;
namespace VanityDAL
{
    public class ConnectionFactory
    {
        public string ConnectionString
        {
            get
            {
                return ConfigurationManager.AppSettings["Vanity"].ToString();
            }
        }
        public SqlConnection GetOpenSqlConnection()
        {
            SqlConnection con = new SqlConnection();
            con.ConnectionString = this.ConnectionString;
            con.Open();
            return con;
        }
    }
}
